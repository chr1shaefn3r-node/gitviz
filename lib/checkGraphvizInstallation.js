"use strict";

var log = require('debug')("gitviz");
var whereis = require('whereis');

var prerequisiteChecker = module.exports = function(onError) {
	var name = "graphviz";
	whereis(name, whereIsCallbackHandling.bind(null, name, onError));
	name = "dot";
	whereis(name, whereIsCallbackHandling.bind(null, name, onError));
};

function whereIsCallbackHandling(name, onError, err, path) {
	if(err || !path) {
		onError(name);
	}
};

