# Gitviz

>  Visualizing the git object database

![] (sample.gif)

## CLI

```sh
$ npm install --global gitviz
```

```sh
$ gitviz --help

  Visualizing the git object database

  Usage:
    gitviz PATH

  Options:
	-w, --watch        watch the PATH for filechanges
    -h, --help         print usage information
    -v, --version      show version info and exit

  Examples:
    $ gitviz /path/to/git/project
```

## External dependencies
 * Graphviz (sudo apt-get install graphviz or brew install graphviz)

# Open issues
 * Add pack-support

## License

MIT © [Christoph Häfner](https://christophhaefner.de)

