"use strict";

var graphviz = require('graphviz'),
	Repo = require('nodegit').Repository;
var visualize = require('./lib/visualizer.js');
var checkGraphvizInstallation = require('./lib/checkGraphvizInstallation.js');

module.exports = function(path) {
	checkGraphvizInstallation(function onError(name) {
		console.error("Error: Couldn't find '"+name+"' binary, please install \"graphviz\" package.");
		process.exit(1);
	});
	var g = graphviz.digraph("G");
	Repo.open(path)
	.then(visualize.bind(null, path, g))
	.then(function() {
		g.output( "pdf", "git-internals.pdf" );
	})
	.catch(function(err) {
		if(err) {
			console.error(err);
		}
	});
};

